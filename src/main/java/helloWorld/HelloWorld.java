package helloWorld;

import org.immutables.value.Value;

@Value.Immutable
public abstract class HelloWorld {

	@Value.Default
	public String helloWorld() {
		return "Hello, world!";
	}

}
