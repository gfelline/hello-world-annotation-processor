package helloworld;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import helloWorld.ImmutableHelloWorld;

public class HelloWorldTest {

	
	@Test
	public void testHelloWorld() throws Exception {
		String result = ImmutableHelloWorld.builder().build().helloWorld();
		assertEquals(result, "Hello, world!");
	}
	
	
}

